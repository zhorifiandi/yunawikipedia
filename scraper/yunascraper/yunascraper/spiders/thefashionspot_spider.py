import unicodedata
import scrapy


class TFSSpider(scrapy.Spider):
    name = "thefashionspot"

    def start_requests(self):
        start_urls = []
        with open("/Users/zhorifiandi/Documents/PARTTIME/yuna/yunawikipedia/scraper/yunascraper/raw/sitemap.txt") as f:
            acontent = f.readlines()
            # you may also want to remove whitespace characters like `\n` at the end of each line
            start_urls = [x.strip() for x in acontent]


        for url in start_urls:
            yield scrapy.Request(url=url, callback=self.parse)

    def parse(self, response):
        item = {}
        # Description
        raw_desc = response.css("div.article-content p::text").extract()
        description = ""
        for rd in raw_desc:
            description += " " + unicodedata.normalize('NFKC',rd)

        item['description'] = description.replace(","," ").strip()
        item['title'] = str(response.css("h1.article-title a::text").extract_first()).replace(","," ").strip()
        # item['author'] = str(response.css("a.article-author::text").extract_first())

        # item['images'] = []
        # for img in response.css("img.lazy::attr('data-lazy-src')").extract():
        #     item['images'].append("http://www.thefashionspot.com" + img)
        # item['replies'] = []
        # item['date'] = str(response.css("time.article-date::attr('datetime')").extract_first())
        # item['categories'] = str(response.css("div.article-crumbs a::text").extract_first())
        return item
