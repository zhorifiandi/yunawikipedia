import sys
from BeautifulSoup import BeautifulSoup as Soup

def parseLog(filename):
    file = filename
    handler = open(file).read()
    soup = Soup(handler)
    for message in soup.findAll('loc'):
        print message.text+","


if __name__ == "__main__":
    parseLog("post-sitemap1.xml")
    parseLog("post-sitemap2.xml")
    parseLog("post-sitemap3.xml")
    parseLog("post-sitemap4.xml")
    parseLog("post-sitemap5.xml")
    parseLog("post-sitemap6.xml")
    parseLog("post-sitemap7.xml")
    parseLog("post-sitemap8.xml")
    parseLog("post-sitemap9.xml")
    parseLog("post-sitemap10.xml")
    parseLog("post-sitemap11.xml")
    parseLog("post-sitemap12.xml")
    parseLog("post-sitemap13.xml")
    parseLog("post-sitemap14.xml")
    parseLog("post-sitemap15.xml")
    parseLog("post-sitemap16.xml")
    parseLog("post-sitemap17.xml")
    parseLog("post-sitemap18.xml")
    parseLog("post-sitemap19.xml")
    parseLog("post-sitemap20.xml")
    parseLog("post-sitemap21.xml")
    parseLog("post-sitemap22.xml")
    parseLog("post-sitemap23.xml")
    parseLog("post-sitemap24.xml")
    parseLog("post-sitemap25.xml")
    parseLog("post-sitemap26.xml")
    parseLog("post-sitemap27.xml")
    parseLog("post-sitemap28.xml")
    parseLog("post-sitemap29.xml")
    parseLog("post-sitemap30.xml")
