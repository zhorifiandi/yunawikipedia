fname = "parsing/girlswithcurve.xml"
with open(fname) as f:
    content = f.readlines()
# you may also want to remove whitespace characters like `\n` at the end of each line
content = [x.strip() for x in content]

print("[")
for line in content:
    if "https" in line:
        my_string = line.split("\t")[0]
        print('"'+my_string+'",')
print("]")
