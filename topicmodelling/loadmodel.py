import pickle
hdp = pickle.load( open( "hdp.model", "rb" ) )

print("\nTOPICS")
for i,topic in enumerate(hdp.show_topics(num_topics=-1)):
    print(i,topic)
