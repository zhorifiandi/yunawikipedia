# Import Gensim Package for Data Processing
from gensim import corpora, models, similarities

#import parallel
# from joblib import Parallel, delayed

import json

pre_dictionary = []
documents = []

d = None
with open('../output.json') as json_data:
    d = json.load(json_data)

for x in d:
    for key in x:
        # print(key)
        # print(x[key])
        # print()
        pre_dictionary.append([key])
        documents.append([x[key]])


dictionary = corpora.Dictionary(pre_dictionary)

# Create corpus from dictionary
corpus = [dictionary.doc2bow(name) for name in documents]

hdp = models.HdpModel(corpus, id2word=dictionary)

# import pdb; pdb.set_trace()
