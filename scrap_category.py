import wikipediaapi
import re
import json


wiki_wiki = wikipediaapi.Wikipedia(
        language='en',
        extract_format=wikipediaapi.ExtractFormat.WIKI
)

DATA_FILENAME = "output.json"

# with open(DATA_FILENAME, mode='w', encoding='utf-8') as f:
#     json.dump([], f)
print('[', file=open(DATA_FILENAME,"w"))
def print_categorymembers(categorymembers, level=0, max_level=2):
        try:
            for c in categorymembers.values():
                # print("%s: %s (ns: %d)" % ("*" * (level + 1), c.title, c.ns))
                # if c.ns != 0:
                text = " ".join(re.findall("[a-zA-Z]+", c.text))
                feeds = []
                # with open(DATA_FILENAME, mode='w', encoding='utf-8') as feedsjson:
                #     entry = {'title': c.title, 'url': text}
                #     feeds.append(entry)
                #     json.dump(feeds, feedsjson)

                print(c.ns)
                print('{"'+c.title+'":"'+text+'"},', file=open(DATA_FILENAME,"a"))
                if c.ns == wikipediaapi.Namespace.CATEGORY and level <= max_level:
                    print_categorymembers(c.categorymembers, level + 1)
        except:
            print("There's an exception, retrying....")
            print_categorymembers(categorymembers, level)



cat = wiki_wiki.page("Category:Fashion")
print("Category members: Category:Fashion")
print_categorymembers(cat.categorymembers)
print('{}]', file=open(DATA_FILENAME,"a"))
