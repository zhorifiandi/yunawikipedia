# YunaWikipedia

### Installation

To run this project easier, install [virtualenv](https://virtualenv.pypa.io/en/stable/).

Install the dependencies and devDependencies and start the server.

```sh
$ virtualenv -p python3 env
$ source env/bin/activate
$ pip3 install wikipedia-api
$ python scrap_category.py
```
